<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '用户列表';

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php
		if(Yii::$app->session->get('mrs_id')==1){
		echo Html::a('添加用户', ['create'], ['class' => 'btn btn-success']);
}
        
         ?>
    </p>

    <?php 
    	if(Yii::$app->session->get('mrs_id')==1){
		echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            'username',
            'name',
            'tel',
            'email:email',
            [ 'attribute' => 'data', 'format' => ['date', 'php:Y-m-d H:i:s']],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); 
}else{

	echo GridView::widget([
		'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],

					'id',
					'username',
					'name',
					'tel',
					'email:email',
					[ 'attribute' => 'data', 'format' => ['date', 'php:Y-m-d H:i:s']],

					['class' => 'yii\grid\ActionColumn',
					'template' => '{view} {update}'],
		],
	]);

}

     ?>

</div>
