<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\LogbookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '工作日志';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="logbook-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('发表日志', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
			[ 'attribute' => 'data', 'format' => ['date', 'php:Y-m-d H:i:s']],

			['attribute'=>'user_id','label'=>'用户名','value'=>'user.username'],
			//['attribute'=>'user.username','label'=>'用户名'],
            'title',
			['attribute'=>'content','format'=>'raw'],
            ['class' => 'yii\grid\ActionColumn',
            'template' => '{view}'],
        ],
    ]); ?>

</div>
