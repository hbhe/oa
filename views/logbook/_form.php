<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use app\assets\AppAsset;
use app\widgets\Alert;

/* @var $this yii\web\View */
/* @var $model app\Models\Logbook */
/* @var $form yii\widgets\ActiveForm */
/* $this->registerCssFile('libs/kindeditor/themes/default/default.css');
$this->registerJsFile('libs/kindeditor/kindeditor-min.js');
$this->registerJsFile('libs/kindeditor/lang/zh_CN.js'); */
AppAsset::register($this);
AppAsset::addCss($this,Yii::$app->request->baseUrl."/libs/kindeditor/themes/default/default.css");
AppAsset::addScript($this,Yii::$app->request->baseUrl."/libs/kindeditor/kindeditor-min.js");
AppAsset::addScript($this,Yii::$app->request->baseUrl."/libs/kindeditor/lang/zh_CN.js");

?>


<?php
$js = <<<EOD
	var editor;  
	KindEditor.ready(function(K) {  
    editor = K.create('#contract-body', {  
             allowFileManager : true  
      });
	});  
EOD;
		
?>
<?php $this->registerJs($js,View::POS_END);?>

<div class="logbook-form">

    <?php $form = ActiveForm::begin(); ?>

   

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
   

    <?= $form->field($model, 'content')->textarea(['id'=>'contract-body','class'=>"editor",'style'=>'width:100%;height:600px;visibility:hidden;']) ?>

     

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '发表' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
