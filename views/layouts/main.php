<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => '后台管理',
        'brandUrl' => ['/logbook/index'],
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    //var_dump(Yii::$app->user->isGuest);exit;
  //var_dump(Yii::$app->session->get('mrs_username'));exit;
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => '首页', 'url' => ['/logbook/index']],
        	['label' => '用户管理', 'url' => ['/user/index']],
        	['label' => '我的日志', 'url' => ['/logbook/logbook']],
            //['label' => 'About', 'url' => ['/site/about']],
           // ['label' => 'Contact', 'url' => ['/site/contact']],
            //['label' => 'Author', 'url' => ['/author/index']],
            /* ['label' => '国家', 'url' => ['/country/index'],
				'items'=>[['label'=>'添加文章','url'=>['/country/create']
						  ],['label'=>'修改文章','url'=>['/country/create']
						  ],['label'=>'删除文章','url'=>['/country/create']
]]], */
           // ['label' => 'Library', 'url' => ['/library/index']],            
            Yii::$app->session->get('mrs_username') ?
                 [
			'label' => '退出 (' . Yii::$app->session->get('mrs_username') . ')',
			'url' => ['/login'],
			'linkOptions' => ['data-method' => 'post']
										]:
                ['label' => '登录', 'url' => ['/login/index']],
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
        		'homeLink'=>[
        				    'label'=>'首页', //修改默认的Home
        		    'url'=>['logbook/index'], //修改默认的Home指向的url地址
        		 ],
        		
        		
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; 公司内部后台管理 <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
