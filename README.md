OA is a simple information system used by Intenal team.
============================

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the oa application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

the enviroment should support Yii2, for the project is based on Yii2.


INSTALLATION
------------

### Install from an Archive File

Extract the archive file downloaded to a directory named `oa` that is directly under the Web root.

Set cookie validation key in `config/web.php` file to some random secret string:

```php
'request' => [
    // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
    'cookieValidationKey' => '<secret random string goes here>',
],
```

You can then access the application through the following URL:

~~~
http://localhost/oa/web/
~~~


CONFIGURATION
-------------

### Database

1. 建database
CREATE DATABASE oa DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;

2. 编辑配置文件
Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=oa',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];
```

3. 执行命令，安装相关的数据库表
php yii migrate

4. http://localhost/oa/web/


### TODO:

1. bug tracker



Enjoy it!
