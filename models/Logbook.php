<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logbook".
 *
 * @property string $id
 * @property integer $user_id
 * @property string $title
 * @property string $content
 * @property integer $data
 */
class Logbook extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logbook';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'title', 'content', 'data'], 'required'],
            [['user_id', 'data'], 'integer'],
            [['title'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '日志ID',
            'user_id' => '用户ID',
            'title' => '标题',
            'content' => '内容',
            'data' => '发表时间',
        ];
    }
    
     public function getUser() {
     	//var_dump($this->hasMany(User::className(), ['id' => 'user_id']));exit;
    	return $this->hasOne(User::className(), ['id' => 'user_id']);
    } 
}
