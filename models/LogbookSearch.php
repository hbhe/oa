<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Logbook;

/**
 * LogbookSearch represents the model behind the search form about `app\Models\Logbook`.
 */
class LogbookSearch extends Logbook
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'data'], 'integer'],
            [['title', 'content'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $user_id=Yii::$app->session->get('mrs_id');
        if ($user_id == 1) {
            $query = Logbook::find()->orderBy('id desc');
        } else {
            $query = Logbook::find()->where("user_id='$user_id'")->orderBy('id desc');
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'data' => $this->data,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }
    
    public function searchs($params)
    {   
        $user_id=Yii::$app->session->get('mrs_id');
        $query = Logbook::find()->where("user_id='$user_id'")->orderBy('id desc');
        // add conditions that should always apply here
    
        $dataProvider = new ActiveDataProvider([
                'query' => $query,
                ]);
    
        $this->load($params);
    
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
    
        // grid filtering conditions
        $query->andFilterWhere([
                'id' => $this->id,
                'user_id' => $this->user_id,
                'data' => $this->data,
                ]);
    
        $query->andFilterWhere(['like', 'title', $this->title])
        ->andFilterWhere(['like', 'content', $this->content]);
    
        return $dataProvider;
    }
}
