<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property string $id
 * @property string $username
 * @property string $password
 * @property string $name
 * @property string $tel
 * @property string $email
 * @property integer $data
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password', 'name', 'tel', 'email', 'data'], 'required'],
            [['data'], 'integer'],
        	[['username','tel','email'], 'unique', 'message'=>'不能重复'],
            [['username'], 'string', 'max' => 40],
            [['password'], 'string', 'max' => 32,'min' => 6],
            [['name'], 'string', 'max' => 20],
            [['tel'],  'match' , 'pattern'=>'/^1\d{10}$/' , 'message' => '电话号码格式不正确'],
            [['email'], 'email', 'message'=>'邮箱格式不正确'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '用户ID',
            'username' => '用户名',
            'password' => '密码',
            'name' => '真实姓名',
            'tel' => '电话',
            'email' => '邮箱',
        	'data' => '添加时间'
        ];
    }
    public function getLogbook() {
    	
    	
    	$this->hasMany(logbook::className(), ['user_id' => 'id']);
    }
}
