<?php

use yii\db\Schema;
use yii\db\Migration;

class m161129_024619_create_news_table extends Migration
{
    public function up()
    {
    	$this->createTable('news', [
    			'id' => 'pk',
    			'title' => Schema::TYPE_STRING . ' NOT NULL',
    			'content' => Schema::TYPE_TEXT,
    			]);
    }

    public function down()
    {
        echo "m161129_024619_create_news_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
