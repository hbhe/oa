<?php

use yii\db\Schema;
use yii\db\Migration;

class m161129_031657_add_column_country_to_conf_table extends Migration
{
    public function up()
    {
    	$this->addColumn('country', 'data',Schema::TYPE_TIMESTAMP);
    }

    public function down()
    {
        echo 'add column country success!';
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
