<?php

use yii\db\Schema;
use yii\db\Migration;

class m161202_151656_create_table_user extends Migration
{
    public function up()
    {
    	$this->createTable('user', [
    			'id' => $this->primaryKey(),
    			'username' => $this->string(40)->notNull(),
    			'password' => $this->string(32)->notNull(),
    			'name' => $this->string(20)->notNull(),
    			'tel' => $this->integer(11)->notNull(),
    			'email' => $this->string(60)->notNull(),
    			'data' => $this->integer(11)->notNull(),
    			]);
    	$this->insert('user', [
    			'id' => 1,
    			'username' => 'admin',
    			'password' => '0c909a141f1f2c0a1cb602b0b2d7d050',
    			'name' => '管理员',
    			'tel' => '12345678910',
    			'email' => 'admin@qq.com',
    			'data' => 1480488989,
    			]);
    }

    public function down()
    {
    	$this->dropTable('user');
    	
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
