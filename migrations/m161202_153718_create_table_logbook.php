<?php

use yii\db\Schema;
use yii\db\Migration;

class m161202_153718_create_table_logbook extends Migration
{
    public function up()
    {
    	$this->createTable('logbook', [
    			'id' => $this->primaryKey(),
    			'user_id' => $this->integer(11)->notNull(),
    			'title' => $this->string(40)->notNull(),
    			'content' => $this->string(255)->notNull(),
    			'data' => $this->integer(11)->notNull(),
    			]);
    }

    public function down()
    {
        $this->dropTable('logbook');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
